const path = require("path");

module.exports = {
  target: "node",

  stats: "minimal",
  watchOptions: {
    aggregateTimeout: 500,
  },

  entry: {
    main: "./src/server/main.ts",
  },

  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    publicPath: "/",
  },

  resolve: {
    extensions: [".svg", ".png", ".js", ".jsx", ".ts", ".tsx", ".css"],
    alias: {
      app: path.resolve("src/app"),
      shared: path.resolve("src/shared"),
    },
  },

  node: {
    __dirname: false,
  },

  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: true,

              presets: ["@babel/preset-typescript", "@babel/preset-react"],
              plugins: [
                "babel-plugin-react-require",
                "@babel/plugin-syntax-dynamic-import",
                "babel-plugin-emotion",
              ],
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [{ loader: "null-loader" }],
      },
      {
        test: /\.(png|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              emitFile: false,
              outputPath: "images",
              name: "[name].[ext]",
              publicPath: "/images/",
            },
          },
        ],
      },
    ],
  },
};
