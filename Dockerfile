FROM node:12-alpine as builder

WORKDIR /frontend

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build


FROM node:12-alpine

WORKDIR /server

COPY --from=builder /frontend/dist /server/dist
COPY --from=builder /frontend/package.json /server/package.json

EXPOSE 4444

CMD [ "npm", "start" ]
