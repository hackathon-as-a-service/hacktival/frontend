const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebappWebpackPlugin = require("webapp-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  target: "web",

  stats: "minimal",
  watchOptions: {
    aggregateTimeout: 500,
  },

  entry: {
    main: "./src/app/main.ts",
  },

  output: {
    path: path.resolve(__dirname, "dist/www"),
    filename: "js/[name].js",
    publicPath: "/",
  },

  resolve: {
    extensions: [".svg", ".png", ".js", ".jsx", ".ts", ".tsx", ".scss"],
    alias: {
      app: path.resolve("src/app"),
      shared: path.resolve("src/shared"),
    },
  },

  node: {
    __dirname: false,
  },

  externals: {
    canvas: true,
  },

  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: true,

              presets: [
                "@babel/preset-typescript",
                ["@babel/preset-env", { corejs: "3" }],
                "@babel/preset-react",
              ],
              plugins: [
                "babel-plugin-react-require",
                "@babel/plugin-syntax-dynamic-import",
                "babel-plugin-emotion",
              ],
            },
          },
        ],
      },
      {
        test: /\.(scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
          },
          // { loader: "postcss-loader" },
          {
            loader: "sass-loader",
            options: {
              includePaths: ["src/app/styles"],
            },
          },
        ],
      },
      {
        test: /\.(woff)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: "fonts",
              name: "[name].[ext]",
              publicPath: "/fonts",
            },
          },
        ],
      },
      {
        test: /\.(png|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: "images",
              name: "[name].[ext]",
              publicPath: "/images/",
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "src/app/index.html",
      filename: "index.html",
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
    }),
    new WebappWebpackPlugin({
      logo: "./src/app/assets/images/icon.svg",
      cache: true,
      prefix: "/",
      publicPath: "/",
      inject: true,
      favicons: {
        developerURL: null,
      },
    }),
  ],
};
