import fs from "fs";
import path from "path";
import express from "express";
import { load } from "cheerio";
import parseToReact from "html-react-parser";

import { pagePaths } from "shared/pagePaths";
import { renderServer } from "app/render";

const absoluteStaticDirectory = path.resolve(__dirname, "www");

function getDocumentExtension() {
  const indexHtmlString = fs.readFileSync(
    path.join(absoluteStaticDirectory, "index.html")
  );

  const queryIndex = load(indexHtmlString);

  const bodyInnerString = queryIndex("body").html();
  const headInnerString = queryIndex("head").html();

  return {
    bodyExtensions: parseToReact(bodyInnerString),
    headExtension: parseToReact(headInnerString),
  };
}

const { bodyExtensions, headExtension } = getDocumentExtension();

const app = express();

app.use(require("cookie-parser")());

app.get(Object.values(pagePaths), (req, res) => {
  const { history, renderStream } = renderServer(
    req.path,
    bodyExtensions,
    headExtension
  );

  if (history.location.pathname !== req.path) {
    res.redirect(history.location.pathname);
  } else {
    renderStream.pipe(
      res,
      { end: true }
    );

    renderStream.on("end", () => {
      res.end();
    });
  }
});
app.use("/", express.static(absoluteStaticDirectory));

const port = 4444;

app.listen(port, () => {
  console.log(`Server running ${port}`);
});
