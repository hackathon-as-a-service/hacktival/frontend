const pagePaths = {
  auth: "/auth",
  dashboard: "/dashboard",
  inbox: "/inbox",
  home: "/",
};

export { pagePaths };
