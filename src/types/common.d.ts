declare module "*.svg" {
  const content: string;

  export default content;
}

type CustomEventHandler<TEvent> = (event: TEvent) => void;
