import "./homePage.scss";

import { FunctionComponent, useContext } from "react";
import { Link } from "react-router-dom";

import HomeHeroSvg from "app/assets/images/homeHero.svg";
import { HistoryContext } from "app/libs/router";
import { pagePaths } from "shared/pagePaths";

interface HomePageProps {}

const HomePage: FunctionComponent<HomePageProps> = () => {
  const history = useContext(HistoryContext);

  return (
    <div className={"home-page"}>
      <div className={"home-page__hero-container"}>
        <img className={"home-page__hero-container__hero"} src={HomeHeroSvg} />
      </div>
      <div className={"home-page__intro-container"}>
        <div className={"home-page__intro-container__intro"}>
          <div className={"home-page__intro-container__intro__title"}>
            {"Bring home the Bacon."}
          </div>
          <div className={"home-page__intro-container__intro__text"}>
            {
              "Get technical leads with an active sourcing tool powered by machine learning."
            }
          </div>
          <Link
            href={pagePaths.dashboard}
            className={"home-page__intro-container__intro__get-started"}
          >
            {"Get Started"}
          </Link>
        </div>
      </div>
    </div>
  );
};

export { HomePage };
