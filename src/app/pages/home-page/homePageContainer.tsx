import { FunctionComponent } from "react";

import { HomePage } from "./homePage";

interface HomePageContainerProps {}

const HomePageContainer: FunctionComponent<HomePageContainerProps> = () => {
  return <HomePage />;
};

export { HomePageContainer };
