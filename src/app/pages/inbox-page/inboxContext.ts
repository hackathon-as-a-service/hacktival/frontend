import { createContext } from "react";

enum InboxFilter {
  None = "none",
  AllLeads = "All Leads",
  Bookmarked = "Bookmarked",
  Contacted = "Contacted",
  Creators = "Creators",
  PotentialFollowers = "Potential Followers",
}

interface InboxContextValue {
  filter: InboxFilter;
}

const InboxContext = createContext<InboxContextValue>({
  filter: InboxFilter.None,
});

export { InboxContext, InboxFilter };
