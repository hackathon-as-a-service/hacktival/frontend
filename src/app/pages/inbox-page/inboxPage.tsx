import "./inboxPage.scss";

import { FunctionComponent } from "react";

import { InboxNavbar } from "./components/inbox-navbar/inboxNavbar";
import { InboxScroll } from "./components/inbox-scroll/inboxScroll";
import { InboxLeadPreview } from "./components/inbox-lead-preview/inboxLeadPreview";
import { InboxFilter } from "./inboxContext";

interface InboxPageProps {
  onChangeFilter: CustomEventHandler<InboxFilter>;
}

const InboxPage: FunctionComponent<InboxPageProps> = ({ onChangeFilter }) => {
  return (
    <div className={"inbox-page"}>
      <InboxNavbar
        className={"inbox-page__navbar"}
        onChangeFilter={onChangeFilter}
      />
      <div className={"inbox-page__main-container"}>
        <div className={"inbox-page__main-container__filter"}>
          {"405 leads (2 filters applied) Filter"}
        </div>
        <main className={"inbox-page__main-container__main"}>
          <InboxScroll className={"inbox-page__main-container__main__scroll"} />
          <InboxLeadPreview
            className={"inbox-page__main-container__main__lead-preview"}
          />
        </main>
      </div>
    </div>
  );
};

export { InboxPage };
