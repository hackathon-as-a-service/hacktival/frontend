import { FunctionComponent, useState, ContextType, useCallback } from "react";
import { produce } from "immer";

import { InboxPage } from "./inboxPage";
import { InboxContext, InboxFilter } from "./inboxContext";

interface InboxPageContainerProps {}

const InboxPageContainer: FunctionComponent<InboxPageContainerProps> = () => {
  const [inboxContext, setInboxContext] = useState<
    ContextType<typeof InboxContext>
  >({
    filter: InboxFilter.AllLeads,
  });

  const handleChangeFilter = useCallback(
    (newFilter: InboxFilter) => {
      if (inboxContext.filter !== newFilter) {
        setInboxContext(
          produce(inboxContext, draft => {
            draft.filter = newFilter;
          })
        );
      }
    },
    [inboxContext]
  );

  return (
    <InboxContext.Provider value={inboxContext}>
      <InboxPage onChangeFilter={handleChangeFilter} />
    </InboxContext.Provider>
  );
};

export { InboxPageContainer };
