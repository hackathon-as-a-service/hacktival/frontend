import "./inboxNavbar.scss";

import {
  FunctionComponent,
  useContext,
  EventHandler,
  MouseEvent,
  useCallback,
} from "react";
import classNames from "classnames";

import IconSvg from "app/assets/images/icon.svg";
import { InboxContext, InboxFilter } from "app/pages/inbox-page/inboxContext";

interface InboxNavbarProps {
  className: string;
  onChangeFilter: CustomEventHandler<InboxFilter>;
}

const InboxNavbar: FunctionComponent<InboxNavbarProps> = ({
  className,
  onChangeFilter,
}) => {
  const { filter } = useContext(InboxContext);

  const handleClickFilter = useCallback(
    (newFilter: InboxFilter) => () => {
      onChangeFilter(newFilter);
    },
    [onChangeFilter]
  );

  return (
    <nav className={classNames(className, "inbox-navbar")}>
      <div className={"inbox-navbar__controls"}>
        <div className={"inbox-navbar__controls__logo"}>
          <img className={"inbox-navbar__controls__logo__icon"} src={IconSvg} />
          <div
            className={"inbox-navbar__controls__logo__text"}
          >{`Inbox${filter && ` - ${filter}`}`}</div>
        </div>
      </div>
      <div className={"inbox-navbar__filters-container"}>
        <InboxNavbarFilter
          onClick={handleClickFilter(InboxFilter.AllLeads)}
          className={"navbar__filters-container__filter"}
          isSelected={filter !== InboxFilter.AllLeads}
        >
          {InboxFilter.AllLeads}
        </InboxNavbarFilter>
        <InboxNavbarFilter
          onClick={handleClickFilter(InboxFilter.Bookmarked)}
          className={"navbar__filters-container__filter"}
          isSelected={filter !== InboxFilter.Bookmarked}
        >
          {InboxFilter.Bookmarked}
        </InboxNavbarFilter>
        <InboxNavbarFilter
          onClick={handleClickFilter(InboxFilter.Contacted)}
          className={"navbar__filters-container__filter"}
          isSelected={filter !== InboxFilter.Contacted}
        >
          {InboxFilter.Contacted}
        </InboxNavbarFilter>
        <InboxNavbarFilter
          onClick={handleClickFilter(InboxFilter.Creators)}
          className={"navbar__filters-container__filter"}
          isSelected={filter !== InboxFilter.Creators}
        >
          {InboxFilter.Creators}
        </InboxNavbarFilter>
        <InboxNavbarFilter
          onClick={handleClickFilter(InboxFilter.PotentialFollowers)}
          className={"navbar__filters-container__filter"}
          isSelected={filter !== InboxFilter.PotentialFollowers}
        >
          {InboxFilter.PotentialFollowers}
        </InboxNavbarFilter>
      </div>
    </nav>
  );
};

export { InboxNavbar };

interface InboxNavbarFilterProps {
  className: string;
  isSelected: boolean;
  onClick: EventHandler<MouseEvent>;
}

const InboxNavbarFilter: FunctionComponent<InboxNavbarFilterProps> = ({
  children,
  className,
  isSelected,
  onClick,
}) => {
  return (
    <button
      onClick={onClick}
      className={classNames(
        className,
        "inbox-navbar-filter",
        isSelected && "inbox-navbar-filter--selected"
      )}
    >
      {children}
    </button>
  );
};
