import "./inboxScroll.scss";

import {
  FunctionComponent,
  useRef,
  useEffect,
  useCallback,
  useState,
} from "react";
import classNames from "classnames";

interface InboxScrollProps {
  className: string;
}

const InboxScroll: FunctionComponent<InboxScrollProps> = ({ className }) => {
  return (
    <div className={classNames(className, "inbox-scroll")}>
      <div className={"inbox-scroll__sort"}>{"test"}</div>
      <InboxScrollLead className={"inbox-scroll__lead"} />
    </div>
  );
};

export { InboxScroll };

interface InboxScrollLeadProps {
  className: string;
}

import { Image } from "app/components/img/img";
import IconSvg from "app/assets/images/icon.svg";
import PointerSvg from "app/assets/images/pointer.svg";

const InboxScrollLead: FunctionComponent<InboxScrollLeadProps> = ({
  className,
}) => {
  const [imageSrc, setImageSrc] = useState("");

  const handleErrorImage = useCallback(() => {
    setImageSrc(IconSvg);
    console.log("tst");
  }, []);

  return (
    <div className={classNames(className, "inbox-scroll-lead")}>
      <div className={"inbox-scroll-lead__image-container"}>
        <Image
          className={"inbox-scroll-lead__image-container__image"}
          src={imageSrc}
          defaultSrc={IconSvg}
        />
      </div>
      <div className={"inbox-scroll-lead__infos"}>
        <div className={"inbox-scroll-lead__infos__language"}>
          {"Javscript"}
        </div>
        <div className={"inbox-scroll-lead__infos__name"}>{"Tammo, Ronke"}</div>
        <div className={"inbox-scroll-lead__infos__location"}>
          <span
            className={"inbox-scroll-lead__infos__location__pointer-container"}
          >
            <img
              className={
                "inbox-scroll-lead__infos__location__pointer-container__pointer"
              }
              src={PointerSvg}
            />
          </span>
          {"Mannheim, Germany"}
        </div>
      </div>
      <div className={"inbox-scroll-lead__status"}>
        <div className={"inbox-scroll-lead__status__select"}>{"X"}</div>
        <div className={"inbox-scroll-lead__status__catch"}>
          {"Absolute Catch"}
        </div>
      </div>
    </div>
  );
};
