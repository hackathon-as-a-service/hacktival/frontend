import "./inboxLeadPreview.scss";

import { FunctionComponent } from "react";
import classNames from "classnames";

interface InboxLeadPreviewProps {
  className: string;
}

const InboxLeadPreview: FunctionComponent<InboxLeadPreviewProps> = ({
  className,
}) => {
  return (
    <div className={classNames(className, "inbox-lead-preview")}>
      {"preview"}
    </div>
  );
};

export { InboxLeadPreview };
