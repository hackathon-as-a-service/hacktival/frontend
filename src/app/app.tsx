import "./app.scss";

import { FunctionComponent, useContext } from "react";
import { Router, Route } from "react-router";

import { pagePaths } from "shared/pagePaths";
import { HistoryContext } from "./libs/router";

import { InboxPageContainer } from "./pages/inbox-page/inboxPageContainer";
import { HomePageContainer } from "./pages/home-page/homePageContainer";

interface AppProps {}

const App: FunctionComponent<AppProps> = ({}) => {
  const history = useContext(HistoryContext);

  if (history) {
    return (
      <Router history={history}>
        <Route exact path={pagePaths.home} component={HomePageContainer} />
        <Route exact path={pagePaths.inbox} component={InboxPageContainer} />
      </Router>
    );
  }

  return null;
};

export { App };
