import { createContext } from "react";
import { History } from "history";

const HistoryContext = createContext<History | null>(null);
const HistoryProvider = HistoryContext.Provider;

export { HistoryContext, HistoryProvider };
