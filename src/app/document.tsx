import { FunctionComponent, ReactNode } from "react";
import { Helmet } from "react-helmet";

interface DocumentProps {
  extendBody: ReactNode;
  extendHead: ReactNode;
}

const Document: FunctionComponent<DocumentProps> = ({
  children,
  extendBody,
  extendHead,
}) => {
  const { htmlAttributes, bodyAttributes, title, link, meta } = Helmet.rewind();

  return (
    <html {...htmlAttributes.toComponent()}>
      <head>
        {title.toComponent()}
        {meta.toComponent()}
        {link.toComponent()}
        {extendHead}
      </head>

      <body {...bodyAttributes.toComponent()}>
        <div id={"root"}>{children}</div>
        {extendBody}
      </body>
    </html>
  );
};

export { Document };
