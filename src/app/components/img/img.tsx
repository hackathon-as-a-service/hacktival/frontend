import "./img.scss";

import { FunctionComponent, ImgHTMLAttributes } from "react";
import classNames from "classnames";

interface ImageProps {
  defaultSrc?: string;
}

const Image: FunctionComponent<
  ImageProps & ImgHTMLAttributes<HTMLImageElement>
> = ({ defaultSrc, src, style, className, ...imgProps }) => {
  return (
    <img
      className={classNames(className, "image")}
      style={{
        ...style,
        backgroundImage: `url("${src}"), ${defaultSrc &&
          `url("${defaultSrc}")`}`,
      }}
      {...imgProps}
    />
  );
};

export { Image };
