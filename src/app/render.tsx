import { ReactNode } from "react";
import { hydrate } from "react-dom";
import { renderToNodeStream } from "react-dom/server";
import { createMemoryHistory, createBrowserHistory } from "history";

import { App } from "./app";
import { Document } from "./document";
import { HistoryProvider } from "./libs/router";

function renderServer(
  pathname: string,
  bodyExtensions: ReactNode,
  headExtensions: ReactNode
) {
  const history = createMemoryHistory({
    initialEntries: [pathname],
    initialIndex: 0,
    keyLength: 6,
  });

  return {
    renderStream: renderToNodeStream(
      <Document extendHead={headExtensions} extendBody={bodyExtensions}>
        <HistoryProvider value={history}>
          <App />
        </HistoryProvider>
      </Document>
    ),
    history: history,
  };
}

function renderClient() {
  const history = createBrowserHistory();
  const root = document.getElementById("root");

  hydrate(
    <HistoryProvider value={history}>
      <App />
    </HistoryProvider>,
    root
  );
}

export { renderClient, renderServer };
